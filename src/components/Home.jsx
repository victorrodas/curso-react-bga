import React, { useEffect } from 'react'
import ListaPropiedades from './ListaPropiedades'
import '../styles/home.css'
import Header from './Header'

const Home = (props) => {
    const [list_data,setListData] = React.useState([]);

    useEffect(()=>{
        setListData(props.data);
    },[])

    const dataFilter = (data)=>{
        let new_data = props.data.filter((value)=>{
            let response = false;
            for (let key in value){
                if(!response){
                    response = String(value[key]).toLowerCase().includes(String(data).toLowerCase());
                }
            }
            return response;
        });
        setListData(new_data);
    }

    return (
        <div className="container-home">
            <Header filter={dataFilter}/>
            <div className="container-title">
                <span className="title-header">{props.title}</span>
                <span className="count">{`${props.data.length}+ stays`}</span>
            </div>
            
            <ListaPropiedades data={list_data}/>
        </div>
    )
}

export default Home
