import React from 'react'
import Propiedad from './Propiedad'
import '../styles/listaPropiedades.css'

const ListaPropiedades = props => {
    return (
        <div className="lista-propiedades">
            {props.data.length>0&&
                props.data.map((element,key) => {
                    return(
                        <Propiedad data={element} key={key} />
                    )
                })
            }
            {props.data.length===0 &&(<div>No se encontraron datos</div>)}
        </div>
    )
}


export default ListaPropiedades
