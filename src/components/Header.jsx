import React from 'react';
import '../styles/header.css'

const Header  = (props) => {
    const [data, setData] = React.useState('');

    return <div className="container-header">
        <div className="">Logo</div>
        <div className="">
            <div className="container-search">
                <div className="container-search-2">
                    Helsinki, Finland
                </div>
                <div className="divider"></div>
                <div className="container-search-2">
                    <input onChange={(event)=>{
                       setData(event.target.value);
                    }}/>
                </div>
                <div className="divider"></div>
                <div className="container-search-2">
                    <button onClick={()=>{ props.filter(data);}}>Buscar</button>
                </div>
            </div>
        </div>
    </div>
}


export default Header;